from time import sleep
from gpiozero import LED
from gpiozero import Button
from random import randint

button=Button(2)
led=LED(19)
print("game starting")
sleep(0.5)
while True:
        sec=randint(1,3)
        sleep(sec)
        led.on()
        x=1
        while x==1:
            if button.is_pressed:
                x=x+1
                print("you got it!")
                led.off()